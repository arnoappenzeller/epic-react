FROM python:3.8-alpine

RUN pip install flask flask_cors

ENV PYTHON_ENV=production

COPY server /usr/src/app

COPY build /usr/src/app/build

WORKDIR /usr/src/app

EXPOSE 8080

CMD [ "python", "./run.py" ]