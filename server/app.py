
from flask import Flask, jsonify
from random import seed
from random import randint
seed(1)

app = Flask(__name__, static_folder='build', static_url_path='')

@app.route('/')
def root():
    return app.send_static_file('index.html')

@app.route('/api/hello')
def hello():
    return "Hello World"

@app.route('/api/stats')
def stats():
    data = [{
        "id": "first",
        "x": randint(0, 100),
        "y": randint(0, 100),
        "label": "React",
        "color": "#61dafb"
    }, {
        "id": "second",
        "x": randint(0, 100),
        "y": randint(0, 100),
        "label": "Angular",
        "color": "#dd0031"
    }, {
        "id": "first",
        "x": randint(0, 100),
        "y": randint(0, 100),
        "label": "Vue",
        "color": "#4fc08d"
    }]
    return jsonify(data)
